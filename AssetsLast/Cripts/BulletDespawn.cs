using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDespawn : MonoBehaviour
{


    Rigidbody2D m_rigid;
  //public Animator animator;
    public int bulletDame;


    private void Awake()
    {
        m_rigid = GetComponent<Rigidbody2D>();
        m_rigid.mass = 10;

        bulletDame = 50;
 //       animator = GetComponent<Animator>();
    }
    void Start()
    {
        Invoke("Despawn", 1.5f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if(collision.gameObject.GetComponent<EnemyHP>() != null)
            {
                collision.gameObject.GetComponent<EnemyHP>().TakeDame(bulletDame);
            }
            Despawn();
        }
                if (collision.gameObject.CompareTag("Boss"))
        {
            if(collision.gameObject.GetComponent<BossHP>() != null)
            {
                collision.gameObject.GetComponent<BossHP>().TakeDame(bulletDame);
            }
            Despawn();
            
        }
    }

    void Despawn()
    {
        m_rigid.velocity = new Vector3(0, 0, 0);
     //// animator.SetBool("isExplode", true);

        SoundControl.instance.BoomExplodeSound();
        Destroy(gameObject, 0.3f);
    }
}
