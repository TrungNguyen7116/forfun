using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map2_RocketFollow : MonoBehaviour
{
    // Start is called before the first frame update
    float moveSpeed;

    private void Awake()
    {
        moveSpeed = 2.5f;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, PlayerControl.instance.transform.position, Time.deltaTime * moveSpeed);
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player")) 
        {
            PlayerControl.instance.playerHP.Despawn(); 
        }
    }
}
