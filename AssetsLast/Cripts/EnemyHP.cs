using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHP : MonoBehaviour
{
    // Start is called before the first frame update
    public int HP;
    public int gold;
    public GameObject despawnEffect;
    public GameObject article;

    public HealthBar healthBar;
    private void Awake()
    {
        HP = 70;
        gold = 50;
    }
    void Start()
    {
        healthBar.SetMaxHealth(HP);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void TakeDame(int dame)
    {
        SoundControl.instance.EnemyTakeDameSound();
        HP -= dame;
        if (HP <= 0) Despawn();
        healthBar.SetHealth(HP);
    }

    void Despawn()
    {
        PlayerControl.instance.playerHP.AddGold(gold);

        if (despawnEffect != null)
        {
            Instantiate(despawnEffect, transform.position, Quaternion.identity);
        }
        if(article != null)
        {
            Instantiate(article, transform.position, Quaternion.identity);
        }
        SoundControl.instance.EnemyDespawnSound();
        Destroy(gameObject);
    }
}
