using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    // Start is called before the first frame update
    static public PlayerControl instance;
    public PlayerHP playerHP;
    public PlayerShoot playerShoot;

    private void Awake()
    {
        PlayerControl.instance = this;
        playerHP = GetComponent<PlayerHP>();
        playerShoot = GetComponent<PlayerShoot>();
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
