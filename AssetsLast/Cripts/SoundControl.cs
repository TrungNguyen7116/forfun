using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundControl : MonoBehaviour
{
    // Start is called before the first frame update

    public static SoundControl instance;
    public AudioSource audioSource;

    public AudioClip minionAttackSound;
    public AudioClip minionTakeDameSound;
    public AudioClip minionJumpSound; 
    public AudioClip minionDespawnSound;

    public AudioClip boomExplodeSound;
    public AudioClip bananaExplodeSound;

    public AudioClip enemyAttackSound;
    public AudioClip enemyTakeDameSound;
    public AudioClip enemyNormalSound;
    public AudioClip enemyDespawnSound;

    public AudioClip bossAttackSound;
    public AudioClip bossTakeDameSound;   
    public AudioClip bossSummonSound;
    public AudioClip bossNormalSound;
    public AudioClip bossDespawnSound;

    public AudioClip ghostAttackSound;  
    public AudioClip ghostTakeDameSound;
    public AudioClip ghostNormalSound;
    public AudioClip ghostDespawnSound;


    private void Awake()
    {
        instance = this;
    }

    public void MinionAttackSound()
    {
        if(audioSource && minionAttackSound)
        audioSource.PlayOneShot(minionAttackSound);
    }
    public void MinionTakeDameSound()
    {
        if (audioSource && minionTakeDameSound)
            audioSource.PlayOneShot(minionTakeDameSound);
    }
    public void MinionJumpSound()
    {
        if (audioSource && minionJumpSound)
            audioSource.PlayOneShot(minionJumpSound);
    }
    public void MinionDespawnSound()
    {
        if (audioSource && minionDespawnSound)
            audioSource.PlayOneShot(minionDespawnSound);
    }
    public void BoomExplodeSound()
    {
        if (audioSource && boomExplodeSound)
            audioSource.PlayOneShot(boomExplodeSound);
    }
    public void BananaExplodeSound()
    {
        if (audioSource && bananaExplodeSound)
            audioSource.PlayOneShot(bananaExplodeSound);
    }
    public void EnemyAttackSound()
    {
        if (audioSource && enemyAttackSound)
            audioSource.PlayOneShot(enemyAttackSound);
    }
    public void EnemyTakeDameSound()
    {
        if (audioSource && enemyTakeDameSound)
            audioSource.PlayOneShot(enemyTakeDameSound);
    }
    public void EnemyNormalSound()
    {
        if (audioSource && enemyNormalSound)
            audioSource.PlayOneShot(enemyNormalSound);
    }
    public void EnemyDespawnSound()
    {
        if (audioSource && enemyDespawnSound)
            audioSource.PlayOneShot(enemyDespawnSound);
    }
    public void BossAttackSound()
    {
        if (audioSource && bossAttackSound)
            audioSource.PlayOneShot(bossAttackSound);
    }
    public void BossTakeDameSound()
    {
        if (audioSource && bossTakeDameSound)
            audioSource.PlayOneShot(bossTakeDameSound);
    }
    public void BossSummonSound()
    {
        if (audioSource && bossSummonSound)
            audioSource.PlayOneShot(bossSummonSound);
    }
    public void BossNormalSound()
    {
        if (audioSource && bossNormalSound)
            audioSource.PlayOneShot(bossNormalSound);
    }
    public void BossDespawnSound()
    {
        if (audioSource && bossDespawnSound)
            audioSource.PlayOneShot(bossDespawnSound);
    }
    public void GhostAttackSound()
    {
        if (audioSource && ghostAttackSound)
            audioSource.PlayOneShot(ghostAttackSound);
    }
    public void GhostTakeDameSound()
    {
        if (audioSource && ghostTakeDameSound)
            audioSource.PlayOneShot(ghostTakeDameSound);
    }
    public void GhostNormalSound()
    {
        if (audioSource && ghostNormalSound)
            audioSource.PlayOneShot(ghostNormalSound);
    }
    public void GhostDespawnSound()
    {
        if (audioSource && ghostDespawnSound)
            audioSource.PlayOneShot(ghostDespawnSound);
    }

}
