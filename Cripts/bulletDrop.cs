using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletDrop : MonoBehaviour
{
    // Start is called before the first frame update

    int numberAmmo;

    private void Awake()
    {
        numberAmmo = 5;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            PlayerControl.instance.playerShoot.IncreaseAmmo(numberAmmo);
            Destroy(gameObject);
        }
                

    }
}
