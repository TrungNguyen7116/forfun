using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFOController : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject lighting;

    public Transform dropPos;
    Vector3 target;

    bool isDrop;

    float dropTimer, m_dropTime;

 

    float lightningSpeed,
            moveSpeed;


    private void Awake()
    {
        lightningSpeed = -200f;
        isDrop = false;
        dropTimer = 15f;
        moveSpeed = 5;
    }
    void Start()
    {
        AutoFly();

    }

    // Update is called once per frame
    void Update()
    {

        AutoFly();

        m_dropTime -= Time.fixedDeltaTime;
        if(m_dropTime < 0)
        {
            target = PlayerControl.instance.transform.position + new Vector3(0f, 1f, 0f);
            m_dropTime = dropTimer;
            isDrop = true;
        }


        if(isDrop && isOnTarget()) StartCoroutine(DropLightning());
       
    }

    
    IEnumerator DropLightning()
    {

        GameObject newLightning = Instantiate(lighting, dropPos.position, Quaternion.identity);
        newLightning.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, lightningSpeed * Time.fixedDeltaTime);

        target = BossController.instance.bossSkills.UFOPos.position;
        isDrop = false;
        
        yield return new WaitForSeconds(dropTimer);
    }
    
    void AutoFly()
    {
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, target, Time.deltaTime * moveSpeed);
    }

    bool isOnTarget()
    {
        return (transform.position == target);
    }
}
