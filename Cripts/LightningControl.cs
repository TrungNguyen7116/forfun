using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningControl : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject despawnEffect;
    Rigidbody2D m_rigid;

    int lightningDame;

    private void Awake()
    {
        m_rigid = GetComponent<Rigidbody2D>();
        m_rigid.mass = 5;

        lightningDame = 5;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.GetComponent<PlayerHP>() != null)
            {
                collision.gameObject.GetComponent<PlayerHP>().TakeDame(lightningDame);
            }
        }
        Despawn();

    }

    void Despawn()
    {
        if (despawnEffect != null)
        {
            Instantiate(despawnEffect, transform.position, Quaternion.identity);
        }
        Destroy(gameObject);
    }


}
