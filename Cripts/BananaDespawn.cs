using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BananaDespawn : MonoBehaviour
{
    public GameObject explodeAnimation;

    Rigidbody2D m_rigid;

    public int bulletDame;
    // Start is called before the first frame update

    private void Awake()
    {
        m_rigid = GetComponent<Rigidbody2D>();
        m_rigid.mass = 10;

    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if(collision.gameObject.GetComponent<EnemyHP>() != null)
            {
                collision.gameObject.GetComponent<EnemyHP>().TakeDame(bulletDame);
            }


            Despawn();
        }
        else Invoke("Despawn", 2f);
    }

    void Despawn()
    {
        SoundControl.instance.BananaExplodeSound();
        if(explodeAnimation != null)
        {
            Instantiate(explodeAnimation, transform.position, Quaternion.identity);
        }
        Destroy(gameObject);
    }
}
