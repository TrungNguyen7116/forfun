using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostControl : MonoBehaviour
{
    public Transform obstacleDetect;
    public LayerMask groundLayer;

    Rigidbody2D m_rigid;
    float moveSpeed, attackSpeed,
            m_attackTime;
    float distanceToPlayer, range;
    int dame;

    bool isAttack, mustFlyUp;
    // Start is called before the first frame update
    private void Awake()
    {
        m_rigid = GetComponent<Rigidbody2D>();
        m_rigid.mass = 10f;
        moveSpeed = 2f;
        range = 1f;
        isAttack = false;
        attackSpeed = 1.5f;
        dame = 7;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerControl.instance.transform.position.x > transform.position.x && transform.localScale.x < 0
                        || PlayerControl.instance.transform.position.x < transform.position.x && transform.localScale.x > 0)
        {
            Flip();
        }

        distanceToPlayer = Vector3.Distance(PlayerControl.instance.transform.position, transform.position);
       
        if (distanceToPlayer > range && !mustFlyUp)
        {
            Move();
        }

        if ((distanceToPlayer < range + 1f) && !isAttack)
        {
            StartCoroutine(Attack());
        }

        if(mustFlyUp) m_rigid.velocity = new Vector2(0, moveSpeed);

    }
    private void FixedUpdate()
    {
        mustFlyUp = Physics2D.OverlapCircle(obstacleDetect.position, 0.1f, groundLayer);
    }


    IEnumerator Attack() 
    {
        isAttack = true;
        yield return new WaitForSeconds(attackSpeed);
        SoundControl.instance.GhostAttackSound();
        PlayerControl.instance.playerHP.TakeDame(dame);
        isAttack = false;
    }
    void Move()
    {
        Vector3 targerPoint = PlayerControl.instance.transform.position;
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, targerPoint, Time.deltaTime * moveSpeed);
    }
    void Flip()
    {
        transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
    }
}
