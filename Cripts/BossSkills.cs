using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Summoner
public class BossSkills : MonoBehaviour
{
    public Transform summonPos, UFOPos;
    public List<GameObject> enemyList;
    public GameObject enemySummoned;
    public GameObject UFO;

    Rigidbody2D m_rigid;

    bool isUFOSummoned;
    int maxEnemySummoned;
    float summonTimer, m_timeSummon;
    float distanceToPlayer, range;

    private void Awake()
    {
        m_rigid = GetComponent<Rigidbody2D>();
        m_rigid.mass = 200;
        m_rigid.gravityScale = 4;

        enemyList = new List<GameObject>();

        isUFOSummoned = false;
        maxEnemySummoned = 2;
        summonTimer = 20f;
        range = 20;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        distanceToPlayer = Vector2.Distance(transform.position, PlayerControl.instance.transform.position);
        
        m_timeSummon -= Time.fixedDeltaTime;
        if((distanceToPlayer < range) && (m_timeSummon < 0))
        {
            SoundControl.instance.BossSummonSound();
            SummonEnemy();
            m_timeSummon = summonTimer;
        }
        
        if((distanceToPlayer < range) && (!isUFOSummoned))
        {
            SoundControl.instance.BossSummonSound();
            SummonUFO();
        }
        CheckEnemyDead();
    }

    void SummonEnemy()
    {
        if (enemyList.Count > maxEnemySummoned) return;

        GameObject newEnemy = Instantiate(enemySummoned, summonPos.position, Quaternion.identity);
        enemyList.Add(newEnemy);    
    }
    
    void SummonUFO()
    {
        isUFOSummoned = true;

        Instantiate(UFO, UFOPos.position,Quaternion.identity);
    }

    void CheckEnemyDead()
    {
        for(int i = 0; i < enemyList.Count; i++)
        {
            if (enemyList[i] != null) enemyList.RemoveAt(i);
        }
    }
}