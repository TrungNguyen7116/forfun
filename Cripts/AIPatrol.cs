using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPatrol : MonoBehaviour
{

    float walkSpeed, range, 
        distanceToPlayer, timeBetweenShoot,
        shootSpeed;

    [HideInInspector]
    public bool mustPatrol;  
    
    private bool mustTurn;
    bool isShooting;

    int isRight;
    public Rigidbody2D m_rigid;
    public Transform groundCheckPos;
    public LayerMask groundLayer;
    public GameObject bullet;
    public Transform shootPos;

    private void Awake()
    {
        m_rigid = GetComponent<Rigidbody2D>();

        timeBetweenShoot = 1;
        shootSpeed = 3;
        walkSpeed = 100;
        range = 5;
    }
    void Start()
    {
        mustPatrol = true;
    }

    // Update is called once per frame
    void Update()
    {
        isRight = transform.localScale.x > 0 ? 1 : -1;
        if(mustPatrol)
        {
            Patrol();
        }

        distanceToPlayer = Vector2.Distance(transform.position, PlayerControl.instance.transform.position);
        
        if(distanceToPlayer < range)
        {
            if(PlayerControl.instance.transform.position.x > transform.position.x && transform.localScale.x < 0
                || PlayerControl.instance.transform.position.x < transform.position.x && transform.localScale.x > 0)
            {
                Flip();
            }
            mustPatrol = false;
            m_rigid.velocity = Vector2.zero;
            if(!isShooting) StartCoroutine(Shoot());
        }
        else { mustPatrol = true; }

    }

    private void FixedUpdate()
    {
        if (mustPatrol)
        {
            mustTurn = !Physics2D.OverlapCircle(groundCheckPos.position, 0.1f, groundLayer);
        }
    }

    IEnumerator Shoot()
    {
        isShooting = true;
        yield return new WaitForSeconds(timeBetweenShoot);

        SoundControl.instance.EnemyAttackSound();
        GameObject newBullet = Instantiate(bullet, shootPos.position, Quaternion.identity);
        newBullet.GetComponent<Rigidbody2D>().velocity = new Vector2 (shootSpeed * walkSpeed * Time.fixedDeltaTime, 0f);
        newBullet.transform.localScale = new Vector2 (newBullet.transform.localScale.x * isRight, newBullet.transform.localScale.y);
        isShooting = false;
    }

    void Patrol()
    {
        if (mustTurn)
        {
            Flip();
        }
        m_rigid.velocity = new Vector2(walkSpeed * Time.fixedDeltaTime, m_rigid.velocity.y);

    }
    void Flip()
    {
        mustPatrol = false;
        transform.localScale = new Vector2 (transform.localScale.x * -1, transform.localScale.y);
        walkSpeed *= -1;
        mustPatrol = true;
    }
}
