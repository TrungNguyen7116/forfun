using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map2_GameOver : MonoBehaviour
{
    // Start is called before the first frame update
    Rigidbody2D m_rigid;
    int dame;

    private void Awake()
    {
        m_rigid = GetComponent<Rigidbody2D>();
        dame = 30;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player")) PlayerControl.instance.playerHP.TakeDame(dame);
    }
}
