
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public int HP;
    public int bullet = 3;
    public int banana = 15;
    public int HPMax = 10;

    // Start is called before the first frame update
    public static SaveManager instance;
    private const string SAVE_1 = "HPSave";
    private const string SAVE_2 = "NumberBulletSave";
    private const string SAVE_3 = "NumberBananaSave";
    private const string SAVE_4 = "HPMaxSave";


    private void Awake()
    {
        if (SaveManager.instance != null) Debug.LogError("Only 1 save allowed");
        instance = this;    
    }

    private void Start()
    {
        this.LoadSaveGame();
    }
    private void FixedUpdate(){
        if(HP != PlayerControl.instance.playerHP.HP 
        || bullet != PlayerControl.instance.playerShoot.ammoBoom
        || banana != PlayerControl.instance.playerShoot.ammoBanana)
        {
            HP = PlayerControl.instance.playerHP.HP;
            bullet = PlayerControl.instance.playerShoot.ammoBoom;
            banana = PlayerControl.instance.playerShoot.ammoBanana;
        } 
    }
    protected virtual string GetSaveNameHP()
    {
        return SaveManager.SAVE_1;
    }
    protected virtual string GetSaveNameNumberBullet()
    {
        return SaveManager.SAVE_2;
    }
    protected virtual string GetSaveNameNumberBanana()
    {
        return SaveManager.SAVE_3;
    }
        protected virtual string GetSaveNameHPMax()
    {
        return SaveManager.SAVE_4;
    }
    public virtual void LoadSaveGame()
    {
        int _HP = PlayerPrefs.GetInt(SAVE_1);
        int _bullet = PlayerPrefs.GetInt(SAVE_2);
        int _banana = PlayerPrefs.GetInt(SAVE_3);
        HPMax = PlayerPrefs.GetInt(SAVE_4);
        Debug.Log("lOAD SAVE GAME: " + _HP + " " + _bullet + " " + _banana + HPMax);
        PlayerControl.instance.playerHP.HP = _HP;
        PlayerControl.instance.playerShoot.ammoBoom = _bullet;
        PlayerControl.instance.playerShoot.ammoBanana = _banana;
    }
    public virtual void SaveGame()
    {
        Debug.Log("save game");
        int HPSave = HP;
        int HPMaxSave = HPMax;
        int numberBulletSave = bullet;
        int numberBananaSave = banana;
        PlayerPrefs.SetInt(GetSaveNameHP(), HPSave);
        PlayerPrefs.SetInt(GetSaveNameNumberBullet(), numberBulletSave);
        PlayerPrefs.SetInt(GetSaveNameNumberBanana(), numberBananaSave);
        PlayerPrefs.SetInt(GetSaveNameHPMax(), HPMaxSave);
    }

    public void SetHPMaxPlayer(){
        HP = HPMax;
    }
}
