using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    float shootSpeed, shootTimer;
    bool isShooting;
    public int ammoBoom, ammoBanana;
    
    public Transform shootPos;
    public GameObject bullet;
    public GameObject banana;

    UImaneger m_ui;
    // Start is called before the first frame update
 
    private void Awake()
    {
        shootSpeed = 800;
        shootTimer = 0.5f;

    }
    void Start()
    {
        m_ui = FindObjectOfType<UImaneger>();
        m_ui.setBoom(ammoBoom.ToString() );
        m_ui.setBanana(ammoBanana.ToString() );
        isShooting = false;

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && !isShooting)
        {
            StartCoroutine(Shoot());
        }
    }
    
    IEnumerator Shoot()
    {
        int direction()
        {
            if (transform.localScale.x > 0f) return 1;
            else return -1;
        }

        isShooting = true;

        SoundControl.instance.MinionAttackSound();
        if (ammoBoom > 0)
        {

            GameObject newBullet = Instantiate(bullet, shootPos.position, Quaternion.identity);
            newBullet.GetComponent<Rigidbody2D>().velocity = new Vector2(direction() * shootSpeed * Time.fixedDeltaTime, 0f);
            newBullet.GetComponent<BulletDespawn>().bulletDame = 50;
            ammoBoom--;
            m_ui.setBoom(ammoBoom.ToString() );
            // a.setBoom(ammoBoom);
        }
        else if(ammoBanana > 0)
        {
            GameObject newBanana = Instantiate(banana, shootPos.position, Quaternion.identity);
            newBanana.GetComponent<Rigidbody2D>().velocity = new Vector2(direction() * shootSpeed * Time.fixedDeltaTime, 0f);
            newBanana.GetComponent<BulletDespawn>().bulletDame = 25;
            ammoBanana--;
            m_ui.setBanana(ammoBanana.ToString() );
        }

        yield return new WaitForSeconds(shootTimer);
        isShooting = false;
    }

    public void IncreaseAmmo(int numberAmmo)
    {
        ammoBoom += numberAmmo;
    }
}
