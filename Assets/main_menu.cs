using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class main_menu : MonoBehaviour
{
    public float transitionTimes = 1f;
    public Animator transition;
    public void PlayGame()
    {
        SaveManager.instance.SaveGame();
        StartCoroutine(LoadLevel( 1));
    }

     IEnumerator LoadLevel(int levelIn) {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTimes);
        SceneManager.LoadScene(levelIn);
    }
    public void QuitGame()
    {
        Application.Quit();
    }

      public void aboutGame()
    {
        SaveManager.instance.SaveGame();
        StartCoroutine(LoadLevel( 7));
    }
}
