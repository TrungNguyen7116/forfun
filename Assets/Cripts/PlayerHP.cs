using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHP : MonoBehaviour
{
    public int HP;
    public int gold = 100;
    public GameObject despawnEffect;
    UImaneger money;
    bool isDead;


    // private void Awake()
    // {
    //     HP = 100;
    //     gold = 0;
    // }

	// public int maxHealth = 100;
	// public int currentHealth;

	public HealthBar healthBar;
    // Update is called once per frame

    void Start(){
          money = FindObjectOfType<UImaneger>();	
          healthBar.SetMaxHealth(HP);
    }
        void Update()
    {
		// currentHealth = maxHealth;
        if(money != null)
        {
        money.setGold(gold.ToString());
        money.setHP(HP.ToString());
        }

    }
    public void AddGold(int m_gold)
    {
        gold += m_gold;
    }
    public void TakeDame(int dame)
    {
        SoundControl.instance.MinionTakeDameSound();
        HP -= dame;
        money.setHP(HP.ToString());
        if (HP <= 0) Despawn();
        healthBar.SetHealth(HP);
    }

    public void Despawn()
    {
    
        SoundControl.instance.MinionDespawnSound();
        if (despawnEffect != null)
        {
            Instantiate(despawnEffect, transform.position, Quaternion.identity);

        }
        // SetActive(false);
        isDead = true;
        UImaneger.instance.bnGameOver.SetActive(true);
        // Destroy(gameObject);
    }
    public bool IsDead(){
        return isDead;
    }
}
