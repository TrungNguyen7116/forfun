using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHP : MonoBehaviour
{
    // Start is called before the first frame update
    public int HP;
    public int gold;
    public GameObject despawnEffect;
    public GameObject article;
    public HealthBar healthBar;
    public GameObject door;
    private void Awake()
    {
        HP = 500;
        gold = 400;

    }
    void Start()
    {
        healthBar.SetMaxHealth(HP);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void TakeDame(int dame)
    {
        SoundControl.instance.BossTakeDameSound();
        HP -= dame;
        if (HP <= 0) Despawn();
        healthBar.SetHealth(HP);
    }

    void Despawn()
    {
        PlayerControl.instance.playerHP.AddGold(gold);

        if (despawnEffect != null)
        {
            Instantiate(despawnEffect, transform.position, Quaternion.identity);
        }
        if(article != null)
        {
            Instantiate(article, transform.position, Quaternion.identity);
        }
        SoundControl.instance.BossDespawnSound();

        door.SetActive(true);
        Destroy(gameObject);
    }
}
