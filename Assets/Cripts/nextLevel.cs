using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class nextLevel : MonoBehaviour
{
    // Start is called before the first frame update
    public int iLevel;
    // public string sLevelToLoad;

    public bool useIn = false;
    public Animator transition;
    public float transitionTimes = 1f;



    private void OnTriggerEnter2D(Collider2D collision){
        Debug.Log("asd");
        if (collision.gameObject.CompareTag("Player")){
            SaveManager.instance.SaveGame();
            LoadScene();
        }
    }

    void LoadScene(){
        if(useIn){
           StartCoroutine(LoadLevel(iLevel));
        }
        // else{
        //    StartCoroutine(LoadLevel(sLevelToLoad));
        // }
    }

    IEnumerator LoadLevel(int levelIn) {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTimes);
        SceneManager.LoadScene(levelIn);
    }
}
