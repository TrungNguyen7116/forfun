using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapControl : MonoBehaviour
{
    // Start is called before the first frame update

    bool istrapped, isAttacking;
    int dame;
    float timeBetweenAttack;

    private void Awake()
    {
        istrapped = false;
        isAttacking = false;

        dame = 10;
        timeBetweenAttack = 1f;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (istrapped && !isAttacking)
        {
            StartCoroutine(Attack());
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player")) istrapped = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player")) istrapped = false;
    }


    IEnumerator Attack()
    {
        isAttacking = true;
        yield return new WaitForSeconds(timeBetweenAttack);

        PlayerControl.instance.playerHP.TakeDame(dame);
        isAttacking = false;
    }

}
