using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    // Start is called before the first frame update
    float walkSpeed, jumpVelocity;
    int direction;
    bool isGround;

    Rigidbody2D m_rigid;
    public Animator animator;

    private void Awake()
    {
        gameObject.tag = "Player";
        isGround = false;   
        jumpVelocity = 350;
        walkSpeed = 130;

    
        m_rigid = GetComponent<Rigidbody2D>();
        m_rigid.mass = 100;
        m_rigid.gravityScale = 1.5f;
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerControl.instance.playerHP.IsDead()) return;
        direction = (int)Input.GetAxisRaw("Horizontal");
        
        if(animator != null) animator.SetFloat("Speed", Mathf.Abs(direction));

        m_rigid.velocity = new Vector2(walkSpeed * direction * Time.fixedDeltaTime, m_rigid.velocity.y);

 
        if(direction != 0)
        {
            transform.localScale = new Vector2(Mathf.Abs(transform.localScale.x) * direction, transform.localScale.y);
        }
        if (Input.GetKeyDown(KeyCode.W) && isGround) Jump();

        
    }

    private void OnCollisionEnter2D(Collision2D collision)
 
    {
        isGround = true;
    }
    void Jump()
    {
        isGround = false;
        SoundControl.instance.MinionJumpSound();
        m_rigid.velocity = new Vector2(m_rigid.velocity.x, jumpVelocity * Time.fixedDeltaTime);
    }
}


// using UnityEngine;

// public class PlayerMove : MonoBehaviour
// {
//     // Start is called before the first frame update
//     float walkSpeed, jumpVelocity;

//     bool isGround;
//     Rigidbody2D m_rigid;


//     private void Awake()
//     {
//         gameObject.tag = "Player";
//         isGround = false;   
//         jumpVelocity = 350;
//         walkSpeed = 130;

//         m_rigid = GetComponent<Rigidbody2D>();
//         m_rigid.mass = 100;
//         m_rigid.gravityScale = 1.5f;
//     }
//     void Start()
//     {

//     }

//     // Update is called once per frame
//     void Update()
//     {
//         float direction = Input.GetAxisRaw("Horizontal");
  

//         m_rigid.velocity = new Vector2(walkSpeed * direction * Time.fixedDeltaTime, m_rigid.velocity.y);

//         if(direction != 0)
//         {
//             transform.localScale = new Vector2(Mathf.Abs(transform.localScale.x) * direction, transform.localScale.y);
//         }
//         if (Input.GetKeyDown(KeyCode.W) && isGround) Jump();

        
//     }

//     private void OnCollisionEnter2D(Collision2D collision)
 
//     {
//         isGround = true;
//     }
//     void Jump()
//     {
//         isGround = false;
//         m_rigid.velocity = new Vector2(m_rigid.velocity.x, jumpVelocity * Time.fixedDeltaTime);
//     }
// }
