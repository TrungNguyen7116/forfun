
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public string check = "";
    public int HP = 100;
    public int bullet = 10;
    public int banana = 100;
    public int HPMax = 100;
    public int gold = 500;
    // Start is called before the first frame update
    public static SaveManager instance;
    private const string SAVE_100 = "1stTimeAccess";
    private const string SAVE_1 = "HPSave";
    private const string SAVE_2 = "NumberBulletSave";
    private const string SAVE_3 = "NumberBananaSave";
    private const string SAVE_4 = "HPMaxSave";
    private const string SAVE_5 = "GoldSave";

    UImaneger m_ui;

    private void Awake()
    {
        if (SaveManager.instance != null) Debug.LogError("Only 1 save allowed");
        instance = this;    
    }

    private void Start()
    {
        this.LoadSaveGame();
        m_ui = FindObjectOfType<UImaneger>();

    }
    private void FixedUpdate(){
        if(check != "") {
            if(HP != PlayerControl.instance.playerHP.HP 
                || bullet != PlayerControl.instance.playerShoot.ammoBoom
                || banana != PlayerControl.instance.playerShoot.ammoBanana
                || gold != PlayerControl.instance.playerHP.gold)
            {
                HP = PlayerControl.instance.playerHP.HP;
                bullet = PlayerControl.instance.playerShoot.ammoBoom;
                banana = PlayerControl.instance.playerShoot.ammoBanana;
                gold = PlayerControl.instance.playerHP.gold;
            }
        }
           
        if(m_ui != null)  m_ui.setHPMax(HPMax.ToString() );
  
    }

    protected virtual string GetSaveCheck()
    {
        return SaveManager.SAVE_100;
    }
    protected virtual string GetSaveNameHP()
    {
        return SaveManager.SAVE_1;
    }
    protected virtual string GetSaveNameNumberBullet()
    {
        return SaveManager.SAVE_2;
    }
    protected virtual string GetSaveNameNumberBanana()
    {
        return SaveManager.SAVE_3;
    }
    protected virtual string GetSaveNameHPMax()
    {
        return SaveManager.SAVE_4;
    }
    protected virtual string GetSaveNameGold()
    {
        return SaveManager.SAVE_5;
    }
    public virtual void LoadSaveGame()
    {
   
        check = PlayerPrefs.GetString(SAVE_100);
 
        if(check == "") 
        {

            PlayerControl.instance.playerHP.HP = HPMax;
            PlayerControl.instance.playerShoot.ammoBoom = bullet;
            PlayerControl.instance.playerShoot.ammoBanana = banana;
            PlayerControl.instance.playerHP.gold = gold;
            return;
        }

        HP = PlayerPrefs.GetInt(SAVE_1);
        bullet = PlayerPrefs.GetInt(SAVE_2);
        banana = PlayerPrefs.GetInt(SAVE_3);
        gold = PlayerPrefs.GetInt(SAVE_5);

        HPMax = PlayerPrefs.GetInt(SAVE_4);

        PlayerControl.instance.playerHP.HP = HP;
        PlayerControl.instance.playerShoot.ammoBoom = bullet;
        PlayerControl.instance.playerShoot.ammoBanana = banana;
        PlayerControl.instance.playerHP.gold = gold;

    }
    public virtual void SaveGame()
    {
        string save = "ACCESSED";
        int HPSave = HP;
        int HPMaxSave = HPMax;
        int numberBulletSave = bullet;
        int numberBananaSave = banana;
        int goldSave = gold;
        PlayerPrefs.SetInt(GetSaveNameHP(), HPSave);
        PlayerPrefs.SetInt(GetSaveNameNumberBullet(), numberBulletSave);
        PlayerPrefs.SetInt(GetSaveNameNumberBanana(), numberBananaSave);
        PlayerPrefs.SetInt(GetSaveNameHPMax(), HPMaxSave);
        PlayerPrefs.SetInt(GetSaveNameGold(), goldSave);


        PlayerPrefs.SetString(GetSaveCheck(), save);

    }

    public void SetHPMaxPlayer(){
        HP = HPMax;
    }
}
