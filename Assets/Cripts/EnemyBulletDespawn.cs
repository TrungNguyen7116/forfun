using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletDespawn : MonoBehaviour
{
    public GameObject despawnEffect;

    int bulletDame;
    // Start is called before the first frame update

    private void Awake()
    {
        bulletDame = 10;
    }
    void Start()
    {
        Invoke("Despawn", 3f);
    }

    // Update is called once per frame
    void Update()
    {
        Invoke("Despawn", 3f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.GetComponent<PlayerHP>() != null)
            {
                collision.gameObject.GetComponent<PlayerHP>().TakeDame(bulletDame);
                
            }
        }
        Despawn();
        
        
    }

    void Despawn()
    {
        if (despawnEffect != null)
        {
            Instantiate(despawnEffect, transform.position, Quaternion.identity);
        }
        Destroy(gameObject);
    }
}
