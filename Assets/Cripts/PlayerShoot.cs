using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    float shootSpeed, shootTimer;
    bool isShooting;
    public int ammoBoom, ammoBanana;
    
    public Transform shootPos;
    public GameObject bullet;
    public GameObject banana;

    bool isBananaChose, isBoomChose;
    public GameObject bananaBox, boomBox;
    UImaneger m_ui;
    // Start is called before the first frame update
 
    private void Awake()
    {
        shootSpeed = 800;
        shootTimer = 0.5f;
        isBananaChose = true;
        isBoomChose = false;   

    }
    void Start()
    {
        m_ui = FindObjectOfType<UImaneger>();
 
        isShooting = false;

    }

    // Update is called once per frame
    void Update()
    {
        if(m_ui != null)
        {
            m_ui.setBoom(ammoBoom.ToString() );
            m_ui.setBanana(ammoBanana.ToString() );
        }
        


        if(Input.GetKeyDown(KeyCode.Space) && !isShooting)
        {
            StartCoroutine(Shoot());
        }

        if(bananaBox != null) bananaBox.SetActive(isBananaChose);
        if(boomBox != null) boomBox.SetActive(isBoomChose);

        if(Input.GetKeyDown(KeyCode.Q)){
            isBananaChose = true;
            isBoomChose = false;
        }
        if(Input.GetKeyDown(KeyCode.E)){
            isBananaChose = false;
            isBoomChose = true;
        }
    }
    
    IEnumerator Shoot()
    {
            Debug.Log("23");
        int direction()
        {
            if (transform.localScale.x > 0f) return 1;
            else return -1;
        }

        isShooting = true;

        SoundControl.instance.MinionAttackSound();
        if (ammoBoom > 0 && isBoomChose)
        {
            GameObject newBullet = Instantiate(bullet, shootPos.position, Quaternion.identity);
            newBullet.GetComponent<Rigidbody2D>().velocity = new Vector2(direction() * shootSpeed * Time.fixedDeltaTime, 0f);
        //    newBullet.GetComponent<BulletDespawn>().bulletDame = 50;
            ammoBoom--;
        //    m_ui.setBoom(ammoBoom.ToString() );
            // a.setBoom(ammoBoom);
        }
        if(ammoBanana > 0 && isBananaChose)
        {
            GameObject newBanana = Instantiate(banana, shootPos.position, Quaternion.identity);
            newBanana.GetComponent<Rigidbody2D>().velocity = new Vector2(direction() * shootSpeed * Time.fixedDeltaTime, 0f);
     //       newBanana.GetComponent<BulletDespawn>().bulletDame = 25;
            ammoBanana--;
       //     m_ui.setBanana(ammoBanana.ToString() );
        }

        yield return new WaitForSeconds(shootTimer);
        isShooting = false;
    }

    public void IncreaseAmmo(int numberAmmo)
    {
        ammoBoom += numberAmmo;
    }

}
