using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UImaneger : MonoBehaviour
{
    // Start is called before the first frame update
    static public UImaneger instance ;
    public GameObject bnGameOver;
    public Text numBoom;
    public Text numBanana;
    public Text gold;
    public Text HP;

    public Text HPMax;
    public void setBoom(string txt){
        if(numBoom){
            numBoom.text = txt;
        }
    }

    public void setHPMax(string txt){
        if(HPMax){
            HPMax.text = txt;
        }
    }
       public void setHP(string txt){
        if(HP){
            HP.text = txt;
        }
    }
    public void setBanana(string txt){
        if(numBanana){
            numBanana.text = txt;
        }
    }

     public void setGold(string txt){
        if(gold){
            gold.text = txt;
        }
    }
    private void Awake(){
        UImaneger.instance = this;
        this.bnGameOver = GameObject.Find("gameOver");
        this.bnGameOver.SetActive(false);
    }
    
}
