using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class pauseMenu : MonoBehaviour
{
   public static bool GameIsPause = false;

    public GameObject pauseMenuUI;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)){
            Debug.Log("1232");
            if(GameIsPause){
                Resume();
            } 
            else
            {
                Pause();
            }
            
        }
    }
    // public void Restart(){
    //     SceneManager.LoadScene( SceneManager.GetActiveScene().name );
    // }
    public void Resume(){
        pauseMenuUI.SetActive(false); 
        Time.timeScale = 1f;
        GameIsPause = false;
    }

    void Pause(){
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPause = true;
    }

    public void LoadMenu(){
        SceneManager.LoadScene("Main_menu");
    }

    public void QuitGame(){
        Application.Quit();
    }

     public void LoadMan(){
        SceneManager.LoadScene(2);
    }
}
