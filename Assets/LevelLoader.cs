using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelLoader : MonoBehaviour
{
    public Animator transition;
    
    public float transitionTime = 5f;

    // // Update is called once per frame
    // void Update()
    // {
    //     if(Input.GetMouseButtonDown()){
    //         SaveManager.instance.SetHPMaxPlayer();
            
    //         SaveManager.instance.SaveGame();
    //         LoadNextLevel();
    //     }
    // }

    public void LoadNextLevel(){
      SaveManager.instance.SetHPMaxPlayer();
            
       SaveManager.instance.SaveGame();
        StartCoroutine(LoadLevel(3));
    }

       public void LoadNext2(){
       SaveManager.instance.SetHPMaxPlayer();
            
       SaveManager.instance.SaveGame();
        StartCoroutine(LoadLevel(4));
    }

    
       public void LoadNext3(){
         SaveManager.instance.SetHPMaxPlayer();
            
       SaveManager.instance.SaveGame();
        StartCoroutine(LoadLevel(5));
    }



    IEnumerator LoadLevel(int levelIndex){
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(levelIndex);
    }   
}
