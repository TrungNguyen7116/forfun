using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shop : MonoBehaviour
{
    // Start is called before the first frame update
    int watterPrice, bulletPrice, HPPrice;

    void Awake()
    {
        watterPrice = 50;
        bulletPrice = 100;
        HPPrice = 200;
    }
    public void buyWater()
    {
        if(SaveManager.instance.gold >= watterPrice)
        {
        PlayerControl.instance.playerShoot.ammoBanana += 10;
        PlayerControl.instance.playerHP.gold -= watterPrice;

    //    SaveManager.instance.SaveGame();
        }
    }
    public void buyBullet()
    {
        if(SaveManager.instance.gold >= bulletPrice)
        {
        PlayerControl.instance.playerShoot.ammoBoom += 10;
        PlayerControl.instance.playerHP.gold -= bulletPrice;
        SaveManager.instance.SaveGame();
        }
    }
    public void buyHPMax()
    {
        if(SaveManager.instance.gold >= HPPrice)
        {  
        SaveManager.instance.HPMax += 10;
        PlayerControl.instance.playerHP.gold -= HPPrice;
        SaveManager.instance.SaveGame();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
