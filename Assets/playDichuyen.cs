using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playDichuyen : MonoBehaviour
{
    // Start is called before the first frame update
    public CharacterController2D controller;
	public Animator animator;

    public float runSpeed = 40f;

	float horizontalMove = 0f;
    bool jump = false;
    	void Update () {

		horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
		
		animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

		if (Input.GetButtonDown("Jump"))
		{
			jump = true;
		}

		// if (Input.GetButtonDown("Crouch"))
		// {
		// 	crouch = true;
		// } else if (Input.GetButtonUp("Crouch"))
		// {
		// 	crouch = false;
		// }

	}

	void FixedUpdate ()
	{
		// Move our character
		controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
		jump = false;
	}
}
